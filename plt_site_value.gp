reset
set macros
set datafile separator ","



file="'test_biomass_v2.csv'"
coln='3'

set palette rgb 33,13,10
stats @file using @coln

set xlabel 'Longtiude'
set ylabel 'Latitude'
set cblabel 'v/Stddev(v)'

plot [-100:0][-30:30] \
                'world_10m_comma.txt' w l noti lc 'black', \
                 @file using 2:1:($3/STATS_stddev) w p noti lt -1 pt 7 ps 5 lc palette



                 #'' using 2:1:($3/STATS_stddev) with points lt 1 pt 6 ps variable lc -1

